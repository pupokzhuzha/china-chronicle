package com.China.Chronicle.test.brain.quiz.app.work

import com.China.Chronicle.test.brain.quiz.app.R


object ChinaGeneration {

    fun gen(page : Int) : ChinaData{
        return when(page){
            1 -> ChinaData(R.drawable.china, "What is the full official name of this country?", "Democratic Republic of China", "People's Republic of China", "Independent Republic of China", "People's Republic of China")
            2 -> ChinaData(R.drawable.stars, "How many stars are there on the national flag of China?", "3", "4", "5", "5")
            3 -> ChinaData(R.drawable.place, "What place in the world does China occupy by territory?", "3", "2", "1", "3")
            4 -> ChinaData(R.drawable.population, "What is the self-name of the Chinese population?", "Han", "Yan", "Tench", "Han")
            5 -> ChinaData(R.drawable.parties, "How many parties officially exist in China?", "1", "2", "3", "1")
            6 -> ChinaData(R.drawable.organisation, "Which organization does the state of China not belong to?", "SCO", "ASEAN", "APEC", "ASEAN")
            7 -> ChinaData(R.drawable.mongol, "What is the name of the Mongol dynasty of emperors that ruled China from 1280 to 1368?", "Min", "Qing", "Yuan", "Yuan")
            8 -> ChinaData(R.drawable.currency, "Which currency is not the official currency of China?", "Renminbi", "Yuan", "Jena", "Jena")
            9 -> ChinaData(R.drawable.invented, "What hasn't been invented in China?", "Paper", "Gunpowder", "Wheel", "Wheel")
            else -> ChinaData(R.drawable.sea, "What sea is China washed by?", "Yellow", "Barents", "Aegean", "Yellow")
        }
    }
}