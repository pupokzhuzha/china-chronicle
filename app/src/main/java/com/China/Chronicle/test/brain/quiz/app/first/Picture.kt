package com.China.Chronicle.test.brain.quiz.app.first

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.China.Chronicle.test.brain.quiz.app.MainActivity
import com.China.Chronicle.test.brain.quiz.app.R
import com.China.Chronicle.test.brain.quiz.app.work.replaceActivity

class Picture : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)

        Handler(Looper.getMainLooper()).postDelayed({
            this.replaceActivity(MainActivity())
        }, 500)
    }
}